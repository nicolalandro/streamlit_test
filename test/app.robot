*** Settings ***
Library  SeleniumLibrary
Library  Process

Suite Setup         Start the webserver

Suite Teardown      Stop the webserver

*** Keywords ***
Start the webserver
    Log To Console  start
    ${process}=     Start Process       python3     -m      coverage    run     --source    src    -m      streamlit    run     src/app.py     --server.port       5000    --server.headless   true

    Set suite variable    ${process}
    Log To Console     ${process}
    sleep  2s

Stop the webserver
    Log To Console  end
    Terminate Process    ${process}


*** Variables ***
${URL}             http://localhost:5000/
${BROWSER}         headlessfirefox

*** Test Cases ***


first test
    Log To Console  test1
    Open Browser  ${URL}  browser=${BROWSER}
    Wait Until Page Contains    Test
    Page Should Contain     You selected comedy.
    Click Element       //*[contains(text(),'Drama')]
    sleep   1
    Page Should Contain     You didn't select comedy.
    Close Browser
