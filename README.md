[![pipeline status](https://gitlab.com/nicolalandro/streamlit_test/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/streamlit_test/-/commits/master)
[![coverage report](https://gitlab.com/nicolalandro/streamlit_test/badges/master/coverage.svg)](https://gitlab.com/nicolalandro/streamlit_test/-/commits/master)

# Testing streamlit

This codebase contains an example to test a streamlit app with selenium and robotframework.

## Run the streamlit app

```
python3.8 -m streamlit run src/app.py --server.port 5000
```

## Run tests

* prepare firefox driver
```
export PATH=$PATH:/home/opensuse/Projects/streamlit_test/driver/.
# or copy driver into been
sudo cp driver/geckodriver /bin/
sudo chmod +x /bin/geckodriver
```
* run tests
```
python3.8 -m robot test
# and report coverage
python3.8 -m coverage report
```


# References

* [selenium](https://selenium-python.readthedocs.io/): library to interact by code with browser
* [geckodriver](https://github.com/mozilla/geckodriver/releases): the driver to use firefox with selenium
* [coverage](https://coverage.readthedocs.io/en/6.3.2/#quick-start): to compute test coverage
* [robotframework](https://robotframework.org/): testing library
  * [Process Library](http://robotframework.org/robotframework/latest/libraries/Process.html): robotframework library to run commands on shell
  * [Selenium Library](http://robotframework.org/SeleniumLibrary/): to use selenium from robotframework